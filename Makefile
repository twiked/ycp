CC = gcc

all: client server

client: fonctions.o
	$(CC) -o client client.c fonctions.o

server: fonctions.o
	$(CC) -o server server.c fonctions.o

fonctions.o:
	$(CC) -c fonctions.c
clean:
	rm -f *.o client server
