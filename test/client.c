#define IPSERVER "127.0.0.1"
#define PORTSERVER 58150
#define CMDSIZE 1001

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "fonctions.h"

connexion CONNEXION;

int main()
{
	CONNEXION.state = CLOSED;
	struct sockaddr_in serverAddress, clientAddress;
	int socketID = 0, size = sizeof(serverAddress);
	char command[CMDSIZE] = "";
	tcp_packet packet;
	srand(time(NULL));
	
	print("Definition des adresses client et serveur... ");
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORTSERVER);
	inet_aton(IPSERVER, &(serverAddress.sin_addr));
	clientAddress.sin_family = AF_INET;
	clientAddress.sin_port = htons(0);
	inet_aton(IPSERVER, &(clientAddress.sin_addr));
	print("OK\n");
	
	print("Creation de la socket... ");
	( (socketID = socket(PF_INET, SOCK_DGRAM, 0)) == -1 ) ? err("ERROR[Soket(socket)]") : print("OK\n");
	
	print("Bind de la socket à l'adresse du client... ");
	( bind(socketID, (struct sockaddr*) &clientAddress, sizeof(clientAddress)) == -1 ) ? err("ERROR[Soket(bind)]") : print("OK\n");
	
	print("Connexion au serveur.. ");
	( connect_y(socketID, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) == -1 ) ? err("ERROR[Soket(connect)]") : print("OK\n");
	
	CONNEXION.state = ESTABLISHED;
	
	// Récupération et mise à jour du nouveau port de connexion
	recvfrom(socketID, &packet, sizeof(packet), 0, (struct sockaddr*) &serverAddress, (socklen_t*) &size);
	serverAddress.sin_port = htons(packet.port_source);
	
	// Réception du message d'accueil
	read_y(socketID, &packet);
	
	printf("\nMessage d'accueil : [%s]\n\n", packet.data);
	
	// On tourne tant que la commande envoyée n'est pas 'quit'
	while ( strcmp(command, "quit") != 0 )
	{
		print("client@server$ ");
		
		while ( getString(command, CMDSIZE) == -1 )
		{
			print(" --> ERROR\n");
			print("client@server$ ");
		}
		
		if (strcmp(command, "quit") == 0)
		{
			break;
		}
		
		packet = makePacket(command);
		if ( strlen(command) > 0 && write_y(socketID, &packet, (struct sockaddr*) &serverAddress) == -1 )
		{
			print("\nLe serveur est inaccessible.\n");
			break;
		}
	}
	
	// Fermeture de la connexion
	close_y(socketID, (struct sockaddr*) &serverAddress);
	
	print("\nFermeture de la socket... ");
	close(socketID);
	print("OK");
	
	CONNEXION.state = CLOSED;
	
	print("\nExtinction");
	return 0;
}

