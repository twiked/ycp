#define IPSERVER "127.0.0.1"
#define PORTSERVER 58150
#define BACKLOG 5
#define CMDSIZE 1001

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "fonctions.h"

typedef struct sockaddr_in sockaddr_in;

connexion CONNEXION;

int main()
{
	CONNEXION.state = CLOSED;
	sockaddr_in serverAddress, clientAddress;
	int socketID = 0, comSocketID = 0, flag = 1, pid = -1;
	unsigned int size = sizeof(clientAddress);
	char command[CMDSIZE] = "", sayWelcome[17] = "Welcome";
	tcp_packet packet;
	
	print("Definition de l'adresse du serveur... ");
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(PORTSERVER);
	inet_aton(IPSERVER, &(serverAddress.sin_addr));
	clientAddress.sin_family = AF_INET;
	print("OK\n");
	
	print("Creation de la socket... ");
	( (socketID = socket(PF_INET, SOCK_DGRAM, 0)) == -1 ) ? err("ERROR[Socket(socket)]") : print("OK\n");
	
	print("Overwrite du port... ");
	( setsockopt(socketID, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag)) == -1 ) ? err("ERROR[Socket(setsockopt)]") : print("OK\n");
	
	print("Bind de la socket à l'adresse du serveur... ");
	( bind(socketID, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) == -1 ) ? err("ERROR[Socket(bind)]") : print("OK\n");
	
	print("\nServeur opérationel ! Attente de clients... \n");
	
	CONNEXION.state = LISTEN;
	
	// Le serveur tourne à l'infini
	while ( packet.fin != '1' )
	{
		
		// Si l'on est dans un processus fils
		if ( pid == 0 )
		{
			print("\n > Attente de DATA... ");
			read_y(comSocketID, &packet);
			
			sprintf(command, "%s", packet.data);
			
			// Réception de la commande : affichage
			if ( packet.fin != '1' && strlen(command) > 0 )
			{
				print("\n   --> DATA reçue : ");
				print(command);
			}
			
			// Cas de la commande 'quit' : l'utilisateur se déconnecte
			if ( packet.fin == '1' )
			{
				print("\n > Déconnexion du client");
				
				print("\n > Fermeture de la connexion... ");
				if ( serverClose(comSocketID, (struct sockaddr*) &clientAddress) != -1 )
				{
					print("OK\n") ;
				}
			}
			else
			{
				continue;
			}
			
			// Si l'on arrive là, on est dans le cas du quit. Dans tous les cas, on quitte le processus fils
			exit(0);
			
		}
		else if ( pid != 0 && (comSocketID = accept_y(socketID, (struct sockaddr*) &clientAddress, &size, serverAddress) ) != -1 )
		{
			puts("\nNouvelle connexion ! Création de la socket réussie.");
			
			// Envoi du message de bienvenue
			packet = makePacket(sayWelcome);
			write_y(socketID, &packet, (struct sockaddr*) &clientAddress);
			
			// Fork pour créer un processus dédié au client
			if ((pid = fork()) == 0)
			{
				CONNEXION.state = ESTABLISHED;
			}
			else
			{
				CONNEXION.state = LISTEN;
			}
		}
		
	}
	
	print("\nFermeture de la socket principale... ");
	close(socketID);
	print("OK");
	
	print("\nExtinction.");
	return 0;
}

