#ifndef FONCTIONS_H
#define FONCTIONS_H

typedef enum State
{
    CLOSED,
    LISTEN,
    ESTABLISHED,
    SYNSENT,
    SYNRECEIVED,
    FIN_WAIT_1,
    FIN_WAIT_2,
    CLOSING,
    TIME_WAIT,
    CLOSE_WAIT,
    LAST_ACK
} State;

typedef struct connexion
{
    State state;
    char window[1000];
} connexion;

typedef struct tcp_packet
{
    unsigned short port_source;
    unsigned short port_destination;
    unsigned numero_sequence ;
    unsigned numero_ack ;
    unsigned short window ;
    unsigned short crc ;
    char syn ;
    char fin ;
    char ack ;
    char rst ;
    char data [1000] ;
} tcp_packet;

int close_y(int socketID, struct sockaddr* address);

int serverClose(int socketID, struct sockaddr* address);

int accept_y(int sockfd, struct sockaddr *addr, socklen_t *addrlen, struct sockaddr_in serverAddress);

int connect_y(int sockfd, struct sockaddr *addr, socklen_t addrlen);

ssize_t read_y(int socketID, tcp_packet *buffer);
ssize_t write_y(int socketID, tcp_packet *buffer, struct sockaddr* address);

tcp_packet makePacket(char* string);



/*
	Affichage à l'aide de la fonction write.
*/
void print(char text[]);

/*
	perror, avec exit
*/
void err(char text[]);

/*
	fgets amélioré
*/
int getString(char* str, int size);

/*
	Vide le buffer
*/
void emptyBuffer();

/*
	recvfrom amélioré
*/
int receive (int socketID, char buffer[], int size);

/*
	send amélioré
*/
int snd (int socketID, char buffer[], int size);

#endif 
