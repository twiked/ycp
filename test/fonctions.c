#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include "fonctions.h"

// #define MAX_SIZE 1000
// #define MAX_CONNECTIONS 100

// typedef struct connexion
// {
// 	pthread_t pthread_id;
// 	int socket_id;
// 	char buffer_read[MAX_SIZE];
// 	char buffer_write[MAX_SIZE];
// 	State etat;
// } connexion;

// connexion tabConnexion[MAX_SIZE];
// pthread_mutex_t lock_receive;

connexion CONNEXION;

int accept_y(int sockfd, struct sockaddr *addr, socklen_t *addrlen, struct sockaddr_in serverAddress)
{
	// Création du packet
	tcp_packet packet;
	
	// On attend un SYN
	do
	{
		recvfrom(sockfd, &packet, sizeof(packet), 0, addr, addrlen);
		
	} while (packet.syn != '1');
	
	CONNEXION.state = SYNRECEIVED;
	
	// Modification du paquet pour renvoyer un SYN + ACK
	packet.syn = '1';
	packet.ack = '1';
	packet.numero_ack = packet.numero_sequence + 1;
	sendto(sockfd, &packet, sizeof(packet), 0, addr, *addrlen);
	
	// Attente d'un ACK
	do
	{
		recvfrom(sockfd, &packet, sizeof(packet), 0, addr, addrlen);
		
	} while (packet.ack != '1');
	
	int socketID = 0;
	int flag = 1;
	int port = rand()%50000 + 10000;
	srand(time(NULL));
	
	serverAddress.sin_port = htons(port);
	
	// Creation de la socket
	( (socketID = socket(PF_INET, SOCK_DGRAM, 0)) == -1 ) ? err("ERROR[Socket(socket)]") : flag;
	
	// Overwrite du port
	( setsockopt(socketID, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag)) == -1 ) ? err("ERROR[Socket(setsockopt)]") : flag ;
	
	// Bind de la socket à l'adresse du serveur
	( bind(socketID, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) == -1 ) ? err("ERROR[Socket(bind)]") : flag ;
	
	// Envoi du nouveau port
	packet.port_source = port;
	sendto(socketID, &packet, sizeof(packet), 0, addr, *addrlen);
	
	return socketID;
}

// void *recv_thread(void *args)
// {
// 	puts("\nReceive thread...\n");

// 	connexion *connexion_courante = args;
// 	connexion_courante->pthread_id = pthread_self();

// 	return 0;
// }

// void reception_signal(int sig)
// {
// 	int i = 0;

// 	if(sig == SIGUSR1)
// 	{
// 		for (int i = 0; (i < MAX_CONNECTIONS) && (tabConnexion[i]->pthread_id != pthread_self()); i++);

// 		if (tabConnexion[i]->pthread_id == pthread_self())
// 		{
// 			if (pthread_mutex_lock(&lock_receive) != 0)
// 			{
// 				perror("mutex_lock");
// 			}

// 			tcp_packet packet = makePacket(tabConnexion[i]->buffer_write);

// 			int result = sendto(tabConnexion[i]->socket_id, packet, (size_t) sizeof(*packet), 0, address, sizeof(*address));

// 			if (pthread_mutex_unlock(&lock_receive) != 0)
// 			{
// 				perror("mutex_lock");
// 			}
// 		}
// 		else
// 		{
// 			return -1;
// 		}
// 	}

// }

// void *send_thread(void *args)
// {
// 	puts("\nSend thread...\n");

// 	connexion *connexion_courante = args;
// 	connexion_courante->pthread_id = pthread_self();

// 	signal(SIGUSR1, reception_signal);

// 	pthread_kill(pthread_self(), SIGUSR1);

// 	return 0;
// }

int connect_y(int sockfd, struct sockaddr *addr, socklen_t addrlen)
{
	// Création et initialisation du packet
	tcp_packet packet;
	packet.numero_sequence = 0;
	packet.numero_ack = 0;
	packet.syn = '1';

	// Création et initialisation du pthread
	// pthread_t recv_pthread, send_pthread;

	// connexion connexion_courante;
	// connexion_courante.pthread_id = NULL;
	// connexion_courante.socket_id = sockfd;
	// connexion_courante.etat = CLOSED;

	// Création des mutex et des threads
	// if (pthread_mutex_init(&lock_receive, NULL) != 0)
	// {
	// 	perror("pthread_mutex receive");
	// 	return 1;
	// }

	// if (pthread_create(&recv_pthread, NULL, &recv_thread, (void*)&connexion_courante) != 0)
	// {
	// 	perror("pthread_create recv");
	// 	return -1;
	// }

	// if (pthread_create(&send_pthread, NULL, &send_thread, (void*)&connexion_courante) != 0)
	// {
	// 	perror("pthread_create send");
	// 	return -1;
	// }
	
	// Envoie du signal SYN
	sendto(sockfd, &packet, sizeof(packet), 0, addr, addrlen);
	
	CONNEXION.state = SYNSENT;
	
	// Attente de réception d'un SYN + ACK
	do
	{
		recvfrom(sockfd, &packet, sizeof(packet), 0, NULL, NULL);
		
	} while (packet.syn != '1' || packet.ack != '1');
	
	// Renvoi d'un ACK simple
	packet.syn = '0';
	packet.ack = '1';
	packet.numero_ack++;
	sendto(sockfd, &packet, sizeof(packet), 0, addr, addrlen);

	// pthread_mutex_destroy(&lock_receive);
	
	return 0;
}

int close_y(int socketID, struct sockaddr* address)
{
	socklen_t size = sizeof(*address);
	tcp_packet packet;
	packet.fin = '1';
	
	// Envoi du FIN
	sendto(socketID, &packet, (size_t) sizeof(packet), 0, address, size);
	
	CONNEXION.state = CLOSE_WAIT;
	
	// Attente du ACK + FIN
	do
	{
		recvfrom(socketID, &packet, sizeof(packet), 0, NULL, NULL);
	} while (packet.ack != '1' || packet.fin != '1');
	
	// Envoi du ACK
	packet.ack = '1';
	packet.fin = '0';
	sendto(socketID, &packet, (size_t) sizeof(packet), 0, address, size);
	
	CONNEXION.state = LAST_ACK;
	
	return 0;
}

int serverClose(int socketID, struct sockaddr* address)
{
	socklen_t size = sizeof(*address);
	tcp_packet packet;
	
	CONNEXION.state = CLOSING;
	
	packet.fin = '1';
	packet.ack = '1';
	// Envoi du FIN + ACK
	sendto(socketID, &packet, (size_t) sizeof(packet), 0, address, size);
	
	// Attente du ACK
	do
	{
		recvfrom(socketID, &packet, sizeof(packet), 0, NULL, NULL);
	} while (packet.ack != '1');
	
	close(socketID);
	CONNEXION.state = CLOSED;
	
	return 0;
}


ssize_t read_y(int socketID, tcp_packet *packet)
{
	struct sockaddr address;
	socklen_t size = sizeof(address);
	
	// Réception du message
	int result = recvfrom(socketID, packet, sizeof(*packet), 0, &address, &size);
	
	if ( result == -1 )
	{
		err("ERROR[socket(recvfrom)]");
	}
	
	// Envoi du ACK
	packet->ack = '1';
	result = sendto(socketID, packet, (size_t) sizeof(*packet), 0, &address, size);
	
	if ( result == -1 )
	{
		err("ERROR[socket(sendto)]");
	}
	
	return (ssize_t) sizeof(packet);
}

// ssize_t myWrite(int socketID, const void *buffer, size_t count)
// {
// 	// Ecrire le contenu du buffer correspondant à la connexion courante dans le packet tcp
// }

ssize_t write_y(int socketID, tcp_packet *packet, struct sockaddr* address)
{
	// Envoi du message
	int result = sendto(socketID, packet, (size_t) sizeof(*packet), 0, address, sizeof(*address));
	
	if ( result == -1 )
	{
		err("ERROR[socket(sendto)]");
	}
	
	// Réception du ACK
	result = recvfrom(socketID, packet, sizeof(*packet), 0, NULL, NULL);
	
	if ( result == -1 )
	{
		err("ERROR[socket(recvfrom)]");
	}
	
	return (packet->ack == '1') ? (ssize_t) sizeof(packet) : -1;
}

tcp_packet makePacket(char* string)
{
	tcp_packet packet;
	sprintf(packet.data, "%s", string);
	return packet;
}

/*
	Affichage à l'aide de la fonction write.
*/
void print(char text[])
{
	write(1, text, strlen(text));
}

/*
	perror, avec exit
*/
void err(char text[])
{
	print("\n\n");
	print("--------------------------------------------------------------------------------");
	perror(text);
	print("Exiting...");
	exit(-1);
}

/*
	fgets amélioré
*/
int getString(char* str, int size) {
	
	if ( fgets(str, size, stdin) == NULL ) {
		emptyBuffer();
		return -1;
	}
	
	char *positionEntree = strchr(str, '\n');
	
	if ( positionEntree == NULL )
		emptyBuffer();
	else
		*positionEntree = '\0';
	
	return 0;
}

/*
	Vide le buffer
*/
void emptyBuffer() {
	int c = 0;
	while ( c != '\n' && c != EOF )
		c = getchar();
}



/*
	recvfrom amélioré
*/
int receive (int socketID, char buffer[], int size)
{
	
	int i = 0;
	
	for ( i = 0 ; i < size ; i++ )
		buffer[i] = '\0';
	
	int result = recv(socketID, buffer, size, 0);
	
	if ( result == -1 )
		err("ERROR[socket(recv)]");
	
	return result;
	
}


int snd (int socketID, char buffer[], int size)
{
	
	int result = send(socketID, buffer, size, 0);
	
	if ( result == -1 )
		err("ERROR[socket(send)]");
	
	return result;
	
}
