#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX 1000

int myAccept(int sockfd, (struct sockaddr *) addr, (socklen_t *) addrlen) {
    // wait for a SYN packet, get its seq number as x, then send back
    // a SYN(seq y) + ACK (seq x+1). Then wait for an ACK (with y+1)

    tcp_packet* packet, packet_ack;

    int n = recvfrom(sockfd, packet, sizeof(tcp_packet), 0, (struct sockaddr *) addr, &addr_len);
    int wanted_seq = -1;

    if (n == -1)
    {
        perror("recvfrom");
    }
    else
    {
        // Test de l'arrivée d'un paquet SYN        
        if (packet->syn == '1')
        {
            // Si le paquet est un SYN, envoyer un paquet SYN+ACK
            tcp_packet* packet_syn;
            packet_syn->syn = '1';
            packet_syn->ack = '1';
            packet_syn->numero_sequence++;

            // Envoyer le packet avec sendTo
            
            wanted_seq = packet_syn->numero_sequence + 1;

            // Attendre un paquet ACK

            if (recvfrom(sockfd, packet_ack, sizeof(tcp_packet), 0, (struct sockaddr *) addr, &addr_len);)
            {
                if (packet_ack == '1' && (packet_ack->numero_sequence == wanted_seq))
                {
                    // OK OK OK OK
                }
            }

            
        }
    }

}

int myConnect(int sockfd, struct sockaddr *addr, socklen_t *addrlen) {
    // send SYN with seq = x, wait for SYN(seq y) + ACK (seq = x + 1)
    // then send ACK with y + 1 seq number. I think that you also need 
    // the TCP ports along.

}

ssize_t myRead(int fd, void *buf, size_t count) {
    // Read from window
}

ssize_t myWrite(int fd, const void *buf, size_t count) {

}

unsigned short checksum(void *buf, size_t count) {
    int i;
    unsigned short csum;
    for(i = count; i > 0; i--) {
        csum += buf[i];
    }
}
