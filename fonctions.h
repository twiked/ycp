#ifndef MYTCP_H
#define MYTCP_H

#import <netinet/in.h>

int myAccept(int sockfd, struct sockaddr* addr, socklen_t *addrlen);

int myConnect(int sockfd, struct sockaddr* addr, socklen_t *addrlen);

int myClose(int sockfd);

ssize_t myRead(int fd, void *buf, size_t count);

ssize_t myWrite(int fd, const void *buf, size_t count);

unsigned short checksum(void *buf, size_t count);

#endif
